import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/Services/auth.service';
import { Constant } from '../navbar/constant';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent {

  menus:any=[]=Constant.menus;
  filteredMenus:any[]=[];
  roles: string='';
  
 
 

  constructor(private authService: AuthService, private router: Router) {

    const userData= localStorage.getItem('currentUser');
    console.log(userData)
    let roles: string[] = [];

  // Check if user data is present in local storage
  if (userData != null) {
    // Parse the user data from JSON
    const parsedUser = JSON.parse(userData);

    // Extract and store user roles
    if (parsedUser && parsedUser.user && parsedUser.user.role) {
      roles = [parsedUser.user.role];
    }
  }

  // Now 'roles' contains the user role(s), and you can use it as needed.
  console.log('User Roles:', roles);

  // The rest of your code (filteredMenus logic) can continue using 'roles' variable.
  this.menus.forEach((element: any) => {
    const isRole = element.roles.find((role: any) => roles.includes(role));
    if (isRole !== undefined) {
      this.filteredMenus.push(element);
    }
  });


  }

  navigateTo(path: string) {
    this.router.navigate([path]);}

  logout() {
    this.authService.logout();
    this.router.navigate(['/login']);
  }
}
